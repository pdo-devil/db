<?php
declare(strict_types=1);

namespace PDODevil\DB;

use Psr\Container\ContainerInterface;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class DatabaseConnector
{
    public function __construct(
        private readonly string $name,
        private readonly ContainerInterface $container
    ) {}
}