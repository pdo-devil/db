<?php
declare(strict_types=1);

namespace PDODevil\DB\Connection;

use PDODevil\DB\Factory\PDOFactory;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class QueryHandler
{
    public function __construct(
        private readonly PDOFactory $factory
    ) {}
}