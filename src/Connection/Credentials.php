<?php
declare(strict_types=1);

namespace PDODevil\DB\Connection;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class Credentials
{
    private readonly string $scheme;
    private readonly string $path;

    public function __construct(
        private readonly string $dsn,
        private readonly ?string $username = null,
        private readonly ?string $password = null
    ) {
        list($this->scheme, $this->path) = array_values(parse_url($this->dsn));
    }

    public function getDsn(): string
    {
        return $this->dsn;
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public static function fromConfig(array $config): self
    {
        if (!isset($config['dsn'])) throw new \InvalidArgumentException(
            'Required config key "dsn" not found'
        );

        return new self($config['dsn'], $config['username'] ?? null, $config['password'] ?? null);
    }
}