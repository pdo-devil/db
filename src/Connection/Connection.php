<?php
declare(strict_types=1);

namespace PDODevil\DB\Connection;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class Connection
{
    public function __construct(
        private readonly QueryHandler $master,
        private readonly array $slaves = []
    ) {}
}