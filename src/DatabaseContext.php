<?php
declare(strict_types=1);

namespace PDODevil\DB;

use PDODevil\DB\Factory\DatabaseFactory;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class DatabaseContext
{
    public function __construct(
        private readonly DatabaseFactory $databaseFactory
    ) {}

    public function getDatabase(string $name = DatabaseFactory::DEFAULT_CONNECTION_NAME)
    {

    }
}