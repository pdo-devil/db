<?php
declare(strict_types=1);

namespace PDODevil\DB\Factory\Helpers;

use PDODevil\DB\Connection\Credentials;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class ConnectionConfigNormalizer
{
    public function __construct(
        private readonly string $requireConnectionName,
        private readonly array $dialectsAvailable = []
    ) {}

    public function normalize(array $connections): array
    {
        $this->assertDefaultConnectionIsSet($connections);

        $normalized = [];
        foreach ($connections as $name => $config) {
            $normalized[$name] = $this->normalizeConnection($config);
        }

        return $normalized;
    }


    private function normalizeConnection(array $config): array
    {
        $credentials = Credentials::fromConfig($config);
        $options = $config['options'] ?? [];
        $dialect = $config['dialect'] ?? $credentials->getScheme();

        $this->assertDialectIsAvailable($dialect);

        $slaves = [];
        if (isset($config['slaves'])) foreach ($config['slaves'] as $slave) {
            $slaveCredentials = Credentials::fromConfig($slaves);
            $slaveDialect = $slave['dialect'] ?? $slaveCredentials->getScheme();

            $this->assertSameDialects($dialect, $slaveDialect);

            $slaves[] = [
                'credentials' => $slaveCredentials,
                'options' => $slave['pdo-options'] ?? $options['pdo-connect'] ?? []
            ];
        }

        return [
            'dialect' => $dialect,
            'master' => ['credentials' => $credentials, 'options' => $options],
            'slaves' => $slaves
        ];
    }

    private function assertDefaultConnectionIsSet(array $connections): void
    {
        if (!isset($connections[$this->requireConnectionName])) throw new \InvalidArgumentException(
            sprintf('Default connection "%s" MUST be configured', $this->requireConnectionName)
        );
    }

    private function assertDialectIsAvailable(string $dialect): void
    {
        if (!in_array($dialect, $this->dialectsAvailable)) throw new \InvalidArgumentException(
            sprintf('Unknown dialect "%s"', $dialect)
        );
    }

    private function assertSameDialects(string $master, string $slave): void
    {
        if ($slave !== $master) throw new \InvalidArgumentException(
            sprintf('Slave dialect "%s" MUST be same as master "%s"', $slave, $master)
        );
    }
}