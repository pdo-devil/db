<?php
declare(strict_types=1);

namespace PDODevil\DB\Factory;

use PDODevil\DB\Connection\Connection;
use PDODevil\DB\Connection\Credentials;
use PDODevil\DB\Connection\QueryHandler;
use PDODevil\DB\DatabaseConnector;
use PHPDevil\DI\ContainerBuilder;
use Psr\Container\ContainerInterface;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
abstract class ConnectorFactory
{
    final public function __construct(
        private readonly ContainerBuilder $containerBuilder
    ) {
        $this->init();
    }

    public function createConnector(string $name, array $config): DatabaseConnector
    {
        $slaves = [];
        if (isset($config['slaves'])) foreach ($config['slaves'] as $slave) {
            $slaves[] = $this->createHandlerFromConfig($slave);
        }
        $master = $this->createHandlerFromConfig($config);
        $connection = $this->createConnection($master, $slaves);

        $container = $this->containerBuilder->build();
        $config->set(Connection::class, $connection);
        return $this->wrapConnector($name, $container);
    }

    protected function init(): void
    {
        // do nothing by default
    }

    protected function wrapConnector(string $name, ContainerInterface $container): DatabaseConnector
    {
        return new DatabaseConnector($name, $container);
    }

    protected function createHandlerFromConfig(array $config): QueryHandler
    {
        return $this->createHandler($config['credentials'], $config['options'] ?? []);
    }

    protected function createHandler(Credentials $credentials, array $connectionOptions = []): QueryHandler
    {
        return new QueryHandler($this->createPDOFactory($credentials, $connectionOptions));
    }

    protected function createPDOFactory(Credentials $credentials, array $connectionOptions = []): PDOFactory
    {
        return new PDOFactory($credentials, $connectionOptions);
    }

    protected function createConnection(QueryHandler $master, array $slaves = []): Connection
    {
        return new Connection($master, $slaves);
    }
}