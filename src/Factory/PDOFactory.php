<?php
declare(strict_types=1);

namespace PDODevil\DB\Factory;

use PDODevil\DB\Connection\Credentials;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
class PDOFactory
{
    public function __construct(
        private readonly Credentials $credentials,
        private readonly array $connectionOptions = []
    ) {}

    public function makePDO(): \PDO
    {
        return new \PDO(
            $this->credentials->getDsn(),
            $this->credentials->getUsername(),
            $this->credentials->getPassword(),
            $this->connectionOptions
        );
    }
}