<?php
declare(strict_types=1);

namespace PDODevil\DB\Factory;

use PDODevil\DB\MySQL\MySQLConnectorFactory;
use PHPDevil\DI\ContainerBuilder;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class DialectFactory
{
    private readonly array $dialects;
    private array $instantiated = [];

    private ?ContainerBuilder $containerBuilder = null;


    public function __construct(array $dialects)
    {
        $this->dialects = array_merge($this->getCoreDialects(), $dialects);
    }

    public function getAvailableDialects(): array
    {
        return array_keys($this->dialects);
    }

    public function getConnectorFactory(string $dialect): ConnectorFactory
    {
        $this->assertIsKnownDialect($dialect);
        if (!isset($this->instantiated[$dialect])) {
            $this->instantiated[$dialect] = $this->instantiate($dialect);
        }
        return $this->instantiated[$dialect];
    }

    private function instantiate(string $dialect): ConnectorFactory
    {
        $class = $this->dialects[$dialect];
        return new $class($this->getContainerBuilder());
    }

    private function getContainerBuilder(): ContainerBuilder
    {
        if (null === $this->containerBuilder) {
            $this->containerBuilder = new ContainerBuilder();
            $this->containerBuilder->load(dirname(__DIR__, 2) . '/config/db.defaults.php');
        }
        return clone $this->containerBuilder;
    }

    private function getCoreDialects(): array
    {
        $dialects = [
            'mysql' => MySQLConnectorFactory::class
        ];

        $filtered = [];
        foreach ($dialects as $k=>$v) {
            if (class_exists($v)) $filtered[$k] = $v;
        }

        return $filtered;
    }

    private function assertIsKnownDialect(string $dialect): void
    {
        if (!array_key_exists($dialect, $this->dialects)) throw new \InvalidArgumentException(
            sprintf('Unknown dialect "%s"', $dialect)
        );
    }
}