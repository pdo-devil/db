<?php
declare(strict_types=1);

namespace PDODevil\DB\Factory;

use PDODevil\DB\DatabaseConnector;
use PDODevil\DB\Factory\Helpers\ConnectionConfigNormalizer;

/**
 * @author Alexey Volkov <webwizardry@hotmail.com>
 */
final class DatabaseFactory
{
    const DEFAULT_CONNECTION_NAME = 'main';

    private readonly DialectFactory $dialectFactory;
    private readonly array $connections;

    public function __construct(array $connections, DialectFactory $dialectFactory = null)
    {
        $this->dialectFactory = $dialectFactory ?? new DialectFactory();

        $this->connections = (new ConnectionConfigNormalizer(
            requireConnectionName: self::DEFAULT_CONNECTION_NAME,
            dialectsAvailable: $this->dialectFactory->getAvailableDialects()
        ))->normalize($connections);
    }

    public function createDatabaseConnector(string $name = self::DEFAULT_CONNECTION_NAME): DatabaseConnector
    {
        $this->assertIsKnownDatabase($name);

        return $this->dialectFactory
            ->getConnectorFactory($this->connections[$name]['dialect'])
            ->createConnector($name, $this->connections[$name]);
    }

    private function assertIsKnownDatabase(string $name): void
    {
        if (!isset($this->connections[$name])) throw new \InvalidArgumentException(
            sprintf('Unknown database "%s"', $name)
        );
    }
}